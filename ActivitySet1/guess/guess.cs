using System;

//TODO: Typing 'q' to quit doesn't seem to work. Typing 'Q' works though. (Can be fixed by making everything lowercase and comparing)
//TODO: Add a number of tries the player can try to guess with. eg: "How many tries do you want to have?"
//TODO: Print the current number of tries the user has input to the screen. eg: "Number of tries left: 3", or "You have tried 3 times so far."
//TODO: Let the player start again when the current game finishes - eg: "Do you want to play again?"
//TODO: Remove "magic numbers" and other hard coded ranges (i.e. "1 and 100"), so the range can be changed easily.
//      Currently the secret number doesn't seem to guess numbers all the way to 100, but the last TODO should fix this bug as well.

//TODO: Remove debug-related writelines (eg " - secret_number = ")
//CHECK: Can the if/else string be optimised? Perhaps using a switch/case statement

namespace guess_game
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the amazing Guess A Number game!");
            Console.WriteLine("Guess a number between 1 and 100.");
            Console.WriteLine("Enter 'Q' to quit if you give up.");

            Random random = new Random();
            int secret_number = random.Next(1, 50);
            // This next line is for debugging...
            Console.WriteLine(" - secret_number = " + secret_number.ToString());

            int guess_number = 0; // 0 means no guess yet
            bool playing = true;
            while (playing) {
                Console.Write("What is it?: ");
                string input = Console.ReadLine();
                if (input == "Q") {
                    playing = false;
                }
                else {
                    if (!int.TryParse(input, out guess_number)) {
                        Console.WriteLine("Eh? That's not a number!");
                    }
                    else {
                        if (guess_number < secret_number) {
                            Console.WriteLine("Nope. Too small.");
                        }
                        else if (guess_number > secret_number) {
                            Console.WriteLine("Nope. Too big.");
                        }
                        else {
                            Console.WriteLine("YES! That's the number!");
                            playing = false;
                        }
                    }
                }
            }
            Console.WriteLine("Thanks for playing. Bye bye.");
        }
    }
}
