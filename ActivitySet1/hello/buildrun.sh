#!/bin/bash
echo NOTE: This file has been modified from the original bat file
echo This simple bat file is trying to [compile] the code.

echo Setting a variable to where I think csc.exe is.
csc=/c/Windows/Microsoft.NET/Framework/v4.0.30319/csc.exe

echo Trying to [compile] the .cs code into the .exe file.
$csc /out:hello_v2.exe hello_v2.cs

echo Trying to [run] the code if available. \(Note - last working version will run!\)
hello_v2.exe
