using System;
using SplashKitSDK;

public class Program
{
    public static void Main()
    {
		SplashKit.OpenWindow("Hello World!", 800, 600);
		while (!SplashKit.WindowCloseRequested("Hello World!")) {
			SplashKit.ClearScreen(SplashKit.ColorWhite());
			SplashKit.FillCircle(SplashKit.ColorBlack(), 400, 300, 200);
			SplashKit.DrawText(string.Format("Hello World!"), SplashKit.ColorBlack(), 400, 20);
			SplashKit.RefreshScreen();
			SplashKit.ProcessEvents();
		}
    }
}
