all:
	pdflatex ActivityReport.tex
	mv ActivityReport.log logs/
	mv ActivityReport.aux logs/
	start sumatrapdf ActivityReport.pdf